<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Log_lib {

	public function __construct()
	{
	    $this->CI =& get_instance();
	}
    
	/**
	 * Get Info dinkes Level
	 * @param array $session
	 * @return bool $res.
	 */
	public function logit($username, $keterangan, $user_id = NULL) {
        $this->CI->load->model('Master_model');

        // tambah log
        $date_now = DATE('Y-m-d H:i:s');

        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
          $auth = 'HTTP_CLIENT_IP';
          $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
          $auth = 'HTTP_X_FORWARDED_FOR';
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
          $auth = 'REMOTE_ADDR';
          $ip=$_SERVER['REMOTE_ADDR'];
        }

        // $host =  gethostbyaddr($ip);

        $log = array();
        $log['user_id'] = $user_id;
        $log['username'] = $username;
        $log['keterangan'] = $keterangan;
        $log['dateactivity'] = $date_now;
        $log['type'] = $auth;
        $log['ip_address'] = $ip;
        $log['host'] = "";
        $log['os'] = "-";
        $log['browser'] = "-";

        $insert = $this->CI->Master_model->add_log($log);
        return $insert;
	}

  public function getOS() { 

    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $os_platform  = "Unknown OS Platform";

    $os_array     = array(
                          '/windows nt 10/i'      =>  'Windows 10',
                          '/windows nt 6.3/i'     =>  'Windows 8.1',
                          '/windows nt 6.2/i'     =>  'Windows 8',
                          '/windows nt 6.1/i'     =>  'Windows 7',
                          '/windows nt 6.0/i'     =>  'Windows Vista',
                          '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                          '/windows nt 5.1/i'     =>  'Windows XP',
                          '/windows xp/i'         =>  'Windows XP',
                          '/windows nt 5.0/i'     =>  'Windows 2000',
                          '/windows me/i'         =>  'Windows ME',
                          '/win98/i'              =>  'Windows 98',
                          '/win95/i'              =>  'Windows 95',
                          '/win16/i'              =>  'Windows 3.11',
                          '/macintosh|mac os x/i' =>  'Mac OS X',
                          '/mac_powerpc/i'        =>  'Mac OS 9',
                          '/linux/i'              =>  'Linux',
                          '/ubuntu/i'             =>  'Ubuntu',
                          '/iphone/i'             =>  'iPhone',
                          '/ipod/i'               =>  'iPod',
                          '/ipad/i'               =>  'iPad',
                          '/android/i'            =>  'Android',
                          '/blackberry/i'         =>  'BlackBerry',
                          '/webos/i'              =>  'Mobile'
                    );

    foreach ($os_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $os_platform = $value;

    return $os_platform;
}

public function getBrowser() {

    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $browser        = "Unknown Browser";

    $browser_array = array(
                            '/msie/i'      => 'Internet Explorer',
                            '/firefox/i'   => 'Firefox',
                            '/safari/i'    => 'Safari',
                            '/chrome/i'    => 'Chrome',
                            '/edge/i'      => 'Edge',
                            '/opera/i'     => 'Opera',
                            '/netscape/i'  => 'Netscape',
                            '/maxthon/i'   => 'Maxthon',
                            '/konqueror/i' => 'Konqueror',
                            '/mobile/i'    => 'Handheld Browser'
                     );

    foreach ($browser_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $browser = $value;

    return $browser;
}

	
}