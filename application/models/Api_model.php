<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    public function add_log($data){
        $insert = $this->db->insert('t_log_user',$data);
        return $insert;
    }

    public function listPuskesmas(){
        $this->db->from("mst_puskesmas");
        $this->db->join("mst_kecamatan","mst_puskesmas.id_kecamatan = mst_kecamatan.id_kecamatan");
        $this->db->join("mst_kota","mst_kecamatan.id_kota = mst_kota.id_kota");
        $this->db->join("mst_provinsi","mst_kota.id_provinsi = mst_provinsi.id_provinsi");
        $query = $this->db->get();
        return $query->result();
    }

}