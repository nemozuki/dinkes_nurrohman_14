<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Puskesmas extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
       
        $this->load->model('Api_model');
    }

    public function index_get(){
		// ambil data dari model
        $dataPuskesmas = $this->Api_model->listPuskesmas();
        $jumlahData = count($dataPuskesmas);
        $data = array();

		// jalankan looping
        if ($jumlahData > 0){
            foreach ($dataPuskesmas as $detailPuskesmas) {
                $tmp = array();
                $tmp['id_puskes'] = $detailPuskesmas->id_puskes;
                $tmp['nama_puskesmas'] = $detailPuskesmas->nama_puskesmas;
                $tmp['alamat_puskesmas'] = $detailPuskesmas->alamat_puskesmas;
                $tmp['no_telp_puskesmas'] = $detailPuskesmas->no_telp_puskesmas;
                $tmp['provinsi']['id_provinsi'] = $detailPuskesmas->id_provinsi;
                $tmp['provinsi']['nama_provinsi'] = $detailPuskesmas->nama_provinsi;
                $tmp['kota']['id_kota'] = $detailPuskesmas->id_kota;
                $tmp['kota']['nama_kota'] = $detailPuskesmas->nama_kota;
                $tmp['kecamatan']['id_kecamatan'] = $detailPuskesmas->id_kecamatan;
                $tmp['kecamatan']['nama_kecamatan'] = $detailPuskesmas->nama_kecamatan;
                $data[] = $tmp;
        }
		
		// set output data api
			$this->set_response(
				array(
					"status" => "success",
					"code" => 200,
					"count" => $jumlahData,
					"data" => $data
			), REST_Controller::HTTP_OK); 
        }else{
            $this->set_response(
                array(
                    "status" => "Tidak ada data",
                    "kode" => 404,
                    "count" => 0,
                    "data" => array()
            ), REST_Controller::HTTP_NOT_FOUND); 
        }            
    }

    


    

}
