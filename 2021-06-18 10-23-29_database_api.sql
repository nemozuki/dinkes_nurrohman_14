/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.5.5-MariaDB : Database - db_tes_rekrutmen
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_tes_rekrutmen` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `db_tes_rekrutmen`;

/*Table structure for table `mst_kecamatan` */

DROP TABLE IF EXISTS `mst_kecamatan`;

CREATE TABLE `mst_kecamatan` (
  `id_kecamatan` varchar(6) NOT NULL,
  `nama_kecamatan` varchar(200) DEFAULT NULL,
  `id_kota` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id_kecamatan`) USING BTREE,
  KEY `id_kota` (`id_kota`),
  CONSTRAINT `mst_kecamatan_ibfk_1` FOREIGN KEY (`id_kota`) REFERENCES `mst_kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `mst_kecamatan` */

insert  into `mst_kecamatan`(`id_kecamatan`,`nama_kecamatan`,`id_kota`) values 
('317105','Kemayoran','3171'),
('317203','Pasar Rebo','3172'),
('317401','Setiabudi','3174');

/*Table structure for table `mst_kota` */

DROP TABLE IF EXISTS `mst_kota`;

CREATE TABLE `mst_kota` (
  `id_kota` varchar(4) NOT NULL,
  `nama_kota` varchar(200) DEFAULT NULL,
  `id_provinsi` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_kota`) USING BTREE,
  KEY `id_provinsi` (`id_provinsi`),
  CONSTRAINT `mst_kota_ibfk_1` FOREIGN KEY (`id_provinsi`) REFERENCES `mst_provinsi` (`id_provinsi`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `mst_kota` */

insert  into `mst_kota`(`id_kota`,`nama_kota`,`id_provinsi`) values 
('3171','Jakarta Pusat','31'),
('3172','Jakarta Timur','31'),
('3174','Jakarta Selatan','31');

/*Table structure for table `mst_provinsi` */

DROP TABLE IF EXISTS `mst_provinsi`;

CREATE TABLE `mst_provinsi` (
  `id_provinsi` varchar(10) NOT NULL,
  `nama_provinsi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_provinsi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `mst_provinsi` */

insert  into `mst_provinsi`(`id_provinsi`,`nama_provinsi`) values 
('31','DKI Jakarta');

/*Table structure for table `mst_puskesmas` */

DROP TABLE IF EXISTS `mst_puskesmas`;

CREATE TABLE `mst_puskesmas` (
  `id_puskes` varchar(18) NOT NULL,
  `nama_puskesmas` varchar(100) DEFAULT NULL,
  `alamat_puskesmas` varchar(100) DEFAULT NULL,
  `no_telp_puskesmas` varchar(16) DEFAULT NULL,
  `id_kecamatan` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id_puskes`) USING BTREE,
  KEY `id_kecamatan` (`id_kecamatan`),
  CONSTRAINT `mst_puskesmas_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `mst_kecamatan` (`id_kecamatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `mst_puskesmas` */

insert  into `mst_puskesmas`(`id_puskes`,`nama_puskesmas`,`alamat_puskesmas`,`no_telp_puskesmas`,`id_kecamatan`) values 
('P31710002','Puskesmas Kecamatan Kemayoran','kp Sukasari no 13','021-002002','317105'),
('P31720003','Puskesmas Kecamatan Pasar Rebo','Jl Kalisari no 10','021-003003','317203');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
